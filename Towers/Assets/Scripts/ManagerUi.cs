﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerUi : SingletonMono<ManagerUi>
{
    [SerializeField]
    private Text m_TextComponent;

    public void UpdateTextInfo(string count)
    {
        m_TextComponent.text="Towers: " + count;
    }
}
