﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameObject m_TowerPrefab;
    private Vector3 m_TargetDirection;
    private Vector3 m_StartPosition;
    private float m_DistanceToMove = 0f;
    private bool m_HitSomething = false;

    private void Awake()
    {
        m_StartPosition = transform.position;
    }

    private void Update()
    {
        transform.Translate(m_TargetDirection * Time.deltaTime, Space.World);
        if(Vector3.Distance(transform.position, m_StartPosition) >= m_DistanceToMove)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDestroy()
    {
        if(!m_HitSomething)
        {
            Instantiate(m_TowerPrefab, transform.position, Quaternion.identity);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Tower")
        {
            m_HitSomething = true;
            Destroy(this.gameObject);
        }
    }


    public void SetInitInfo(Vector3 targetDirection, float distance)
    {
        m_DistanceToMove = distance;
        m_TargetDirection = targetDirection;
    }
}
