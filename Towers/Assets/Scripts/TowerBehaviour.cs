﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBehaviour : MonoBehaviour
{
    public static int TowerCount
    {
        get => m_Towercount;
        set
        {
            m_Towercount = value;
            ManagerUi.Instance?.UpdateTextInfo(m_Towercount.ToString());
        }
    }

    private static int m_Towercount = 0;
    [SerializeField]
    private BulletBehaviour m_BulletPrefab;
    [SerializeField]
    private GameObject m_BulletStartPosition;
    [SerializeField]
    private float m_SmoothTime = 3f;
    [SerializeField]
    private bool m_StartTower = false;

    private SpriteRenderer m_SpriteRenderer;
    private Quaternion m_ActualRotation;


    private void Awake()
    {
        TowerCount++;
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        if (m_StartTower)
        {
            SetTowerState(true);
            InvokeRepeating(nameof(RotateRandom), 0.5f, 0.5f);
        }
        else
        {
            SetTowerState(false);
            StartCoroutine(Respawning());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDestroy()
    {
        TowerCount--;
    }

    private void RotateRandom()
    {
        m_ActualRotation = transform.rotation;
        transform.Rotate(new Vector3(0, 0, Random.Range(15f, 45f)));
        BulletBehaviour bulletGo = Instantiate(m_BulletPrefab, m_BulletStartPosition.transform.position, Quaternion.identity);
        Vector3 targetDireciotn = (m_BulletStartPosition.transform.position - transform.position).normalized * 4f; // razy 4 poniewaz predkosc ma miec 4 jednostki/s
        bulletGo.SetInitInfo(targetDireciotn, Random.Range(1f, 4f));
    }

    private IEnumerator Respawning()
    {
        yield return new WaitForSeconds(6.0f);
        SetTowerState(true);
        InvokeRepeating(nameof(RotateRandom), 0.5f, 0.5f);
    }

    private void SetTowerState(bool state)
    {
        if (state)
        {
            m_SpriteRenderer.color = new Color(1f, 0f, 0f);
        }
        else
        {
            m_SpriteRenderer.color = new Color(1f, 1f, 1f);
        }
    }
}
