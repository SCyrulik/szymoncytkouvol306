﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    private bool m_MouseLpmClick = false;

    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Debug.Log("weszlo");
            m_MouseLpmClick = true;
        }
    }

    private void OnMouseUp()
    {
        Debug.Log("Wyszlo");
        m_MouseLpmClick = false;
    }

    private void Update()
    {
        if (!m_MouseLpmClick)
            return;
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Debug.Log("pozycja world " + worldPosition);
        transform.position = new Vector3(worldPosition.x, worldPosition.y, 1);
    }
}
